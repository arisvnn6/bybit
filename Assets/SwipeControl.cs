﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwipeControl : MonoBehaviour
{
    public GameObject scrollBar;
    public Scrollbar scrollBar2;
    public float scroll_pos = 0;
    float[] pos;
    public Image imgTarget;
    public Animator pointerAnim;
    public Sprite[] pointerSprite;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        pos = new float[transform.childCount];
        float distance = 1f / (pos.Length - 1f);
        for(int i=0; i<pos.Length; i++)
        {
            pos[i] = distance * i;
        }

        if(Input.GetMouseButton(0))
        {
            scroll_pos = scrollBar.GetComponent<Scrollbar>().value;
        }
        else
        {
            for(int i=0; i<pos.Length; i++)
            {
                if(scroll_pos < pos[i] + (distance/2) && scroll_pos > pos[i] - (distance/2))
                {
                    scrollBar.GetComponent<Scrollbar>().value = Mathf.Lerp (scrollBar.GetComponent<Scrollbar>().value, pos[i], 0.15f);
                    imgTarget.sprite = pointerSprite[i];
                }
            }
        }

        if(scrollBar2.value<0)
        {
            pointerAnim.Play("PointerShow");
        }
        else
        {
            if (scroll_pos > 0.1)
            {
                pointerAnim.Play("PointerShow");
            }
            else
            {
                pointerAnim.Play("PointerOff");
            }
        }
    }

    public void ChangePos(float pos)
    {
        scroll_pos = pos;
    }
}
